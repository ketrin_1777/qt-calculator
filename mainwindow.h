 #ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QKeyEvent>
#include <QLineEdit>
#include <QTextEdit>



QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    MainWindow(Ui::MainWindow *ui, double lstNumCalc, const QString &strCalc, const QString &symbolCalc, const QString &currentNumCalc);
    ~MainWindow();

protected:
        void keyPressEvent(QKeyEvent *event) override;

private slots:
        void on_buttom_1_clicked();

        void on_buttom_2_clicked();

        void on_buttom_3_clicked();

        void on_buttom_4_clicked();

        void on_buttom_5_clicked();

        void on_buttom_6_clicked();

        void on_buttom_7_clicked();

        void on_buttom_8_clicked();

        void on_buttom_9_clicked();

        void on_buttom_0_clicked();

        void on_buttom_minus_clicked();

        void on_buttom_plus_clicked();

        void on_buttom_backspace_clicked();

        void on_buttom_c_clicked();

        void on_buttom_ce_clicked();


        void addElementsEnd(const char elem);

        void mathematFunct(const char elem);

        void str_equally();

        void backspace_delete();


        void on_buttom_equal_clicked();

        void on_pushButton_2_clicked();

        void on_buttom_divis_clicked();


private:
    Ui::MainWindow *ui;

    double lstNumCalc;

    QString strCalc;

    QString equalCalc;

    QString symbolCalc;

    QString currentNumCalc;

};
#endif // MAINWINDOW_H
