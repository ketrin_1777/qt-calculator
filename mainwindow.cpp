#include "mainwindow.h"
#include "./ui_mainwindow.h"

#include <QtDebug>

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    qApp->installEventFilter(this);

    symbolCalc = nullptr;
    lstNumCalc = 0;



}

MainWindow::~MainWindow()
{
    delete ui;

}


void MainWindow::keyPressEvent(QKeyEvent *event)
{
    switch(event->key()) {
        case Qt::Key_Backspace: backspace_delete(); break;
        case Qt::Key_Minus: mathematFunct('-'); break;
        case Qt::Key_Plus: mathematFunct('+');  break;
        case Qt::Key_multiply: mathematFunct('*'); break;
        case Qt::Key_Backslash: mathematFunct('/'); break;
        case Qt::Key_Equal: str_equally(); break;
        case Qt::Key_1: addElementsEnd('1'); break;
        case Qt::Key_2: addElementsEnd('2'); break;
        case Qt::Key_3: addElementsEnd('3'); break;
        case Qt::Key_4: addElementsEnd('4'); break;
        case Qt::Key_5: addElementsEnd('5'); break;
        case Qt::Key_6: addElementsEnd('6'); break;
        case Qt::Key_7: addElementsEnd('7'); break;
        case Qt::Key_8: addElementsEnd('8'); break;
        case Qt::Key_9: addElementsEnd('9'); break;
        case Qt::Key_0: addElementsEnd('0'); break;
        case Qt::Key_Slash: addElementsEnd('.'); break;

    }

}




void MainWindow::on_buttom_1_clicked()
{
    addElementsEnd('1');
}

void MainWindow::on_buttom_2_clicked()
{
    addElementsEnd('2');
}


void MainWindow::on_buttom_3_clicked()
{
    addElementsEnd('3');
}


void MainWindow::on_buttom_4_clicked()
{
    addElementsEnd('4');
}


void MainWindow::on_buttom_5_clicked()
{
    addElementsEnd('5');
}


void MainWindow::on_buttom_6_clicked()
{
    addElementsEnd('6');
}


void MainWindow::on_buttom_7_clicked()
{
    addElementsEnd('7');
}

void MainWindow::on_buttom_8_clicked()
{
    addElementsEnd('8');
}

void MainWindow::on_buttom_9_clicked()
{
    addElementsEnd('9');
}

void MainWindow::on_buttom_0_clicked()
{
    addElementsEnd('0');
}

void MainWindow::on_buttom_minus_clicked()
{
    mathematFunct('-');
}


void MainWindow::on_buttom_plus_clicked()
{
    mathematFunct('+');
}


void MainWindow::on_pushButton_2_clicked()
{
    mathematFunct('*');
}


void MainWindow::on_buttom_divis_clicked()
{
    mathematFunct('/');
}

void MainWindow::on_buttom_equal_clicked()
{
    str_equally();
}


void MainWindow::on_buttom_backspace_clicked()
{
    backspace_delete();
}


void MainWindow::on_buttom_c_clicked()
{
    backspace_delete();

}

void MainWindow::on_buttom_ce_clicked()
{
    lstNumCalc = 0;
    currentNumCalc.clear();
    strCalc.clear();
    symbolCalc = nullptr;
    ui->textEdit->setText(strCalc);
    equalCalc.clear();
    ui->label->clear();
    equalCalc.insert(equalCalc.size(), "= ");
    ui->label->setText(equalCalc);
}


void MainWindow::addElementsEnd(const char elem){
    currentNumCalc.insert(currentNumCalc.size(), elem);
    strCalc.insert(strCalc.size(), elem);
    ui->textEdit->setText(strCalc);
}

void MainWindow::mathematFunct(const char elem)
{
    if(symbolCalc == nullptr)
    {
        symbolCalc = elem;
        lstNumCalc = currentNumCalc.toDouble();
        currentNumCalc = nullptr;
        strCalc.insert(strCalc.size(), elem);
        ui->textEdit->setText(strCalc);
    }
    else if(currentNumCalc == nullptr)
    {
        symbolCalc = elem;
        strCalc.chop(1);
        strCalc.insert(strCalc.size(), elem);
        ui->textEdit->setText(strCalc);
    }
    else {
        double temp = currentNumCalc.toDouble();
        if(symbolCalc == "+")
        {
           lstNumCalc = lstNumCalc + temp;
        }
        if(symbolCalc == "-")
        {
            lstNumCalc = lstNumCalc - temp;
        }
        if(symbolCalc == "*")
        {
           lstNumCalc = lstNumCalc * temp;
        }
        if(symbolCalc == "/")
        {
            lstNumCalc = lstNumCalc / temp;
        }
        currentNumCalc = nullptr;
        symbolCalc = elem;
        strCalc.insert(strCalc.size(), elem);
        ui->textEdit->setText(strCalc);
    }

}


void MainWindow::str_equally()
{
    if( symbolCalc == nullptr)
    {
        equalCalc.clear();
        ui->label->clear();
        equalCalc.insert(equalCalc.size(), "= ");
        equalCalc.append(currentNumCalc);
        ui->label->setText(equalCalc);
        strCalc.clear();
        lstNumCalc = 0;
    }
    else
    {
        double temp = currentNumCalc.toDouble();
        equalCalc.clear();
        ui->label->clear();
        equalCalc.insert(equalCalc.size(), "= ");
        if(symbolCalc == "+")
        {
           lstNumCalc = lstNumCalc + temp;
        }
        if(symbolCalc == "-")
        {
            lstNumCalc = lstNumCalc - temp;
        }
        if(symbolCalc == "*")
        {
           lstNumCalc = lstNumCalc * temp;
        }
        if(symbolCalc == "/")
        {
            lstNumCalc = lstNumCalc / temp;
        }

        equalCalc.append(QString::number(lstNumCalc));
        ui->label->setText(equalCalc);
        strCalc.clear();
        strCalc.append(QString::number(lstNumCalc));
        strCalc.append(symbolCalc);
        strCalc.append(currentNumCalc);
        ui->textEdit->setText(strCalc);

    }

}

void MainWindow::backspace_delete()
{
    if(equalCalc == "= " || equalCalc == nullptr)
    {
        if(strCalc == nullptr)
        {
            lstNumCalc = 0;
        }
        else if(strCalc.at(strCalc.length() - 1) == "+" ||strCalc.at(strCalc.length() - 1) == "-" || strCalc.at(strCalc.length() - 1) == "*" ||strCalc.at(strCalc.length() - 1) == "/")
        {
               symbolCalc = nullptr;
               currentNumCalc = QString::number(lstNumCalc);
               lstNumCalc = 0;
               strCalc.chop(1);
        }
        else
        {
            currentNumCalc.chop(1);
            strCalc.chop(1);
        }

    }
    else{
        lstNumCalc = 0;
        currentNumCalc.clear();
        strCalc.clear();
        symbolCalc = nullptr;
        ui->textEdit->setText(strCalc);
        equalCalc.clear();
        ui->label->clear();
        equalCalc.insert(equalCalc.size(), "= ");
        ui->label->setText(equalCalc);
    }
    ui->textEdit->setText(strCalc);

}




